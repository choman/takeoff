require 'httparty'
require './lib/shared_status'

class SlackWebhook
  CouldNotPostError = Class.new(StandardError)

  WEBHOOK_URL = 'https://hooks.slack.com/services/'.freeze
  TRUNCATE_MESSAGE_SIZE = 200

  def initialize(environment, version, shared_status)
    @url = WEBHOOK_URL + Takeoff.config[:slack_token]
    @shared_status = shared_status
    @environment = environment
    @version = version
  end

  def deploy_started
    fire_hook("#{header} started the deploy of #{gitlab_info}")
  end

  def deploy_resumed
    fire_hook("#{header} resumed the deploy of #{gitlab_info}")
  end

  def warm_up
    text = <<~SLACK_MESSAGE.strip
      #{header} is warming up takeoff with GitLab *v#{@version}* packages :hotsprings:
    SLACK_MESSAGE

    fire_hook(text)
  end

  def deploy_finished
    text = <<~SLACK_MESSAGE.strip
      #{header} finished deploying #{gitlab_info} after #{total_time} #{fast_emoticon} :party-tanuki:
    SLACK_MESSAGE

    fire_hook(text)
  end

  def deploy_interrupted(step)
    text = <<~SLACK_MESSAGE.strip
      #{header} interrupted the deployment of #{gitlab_info} on #{step.class} after #{total_time} :bomb-tanuki:
    SLACK_MESSAGE

    fire_hook(text)
  end

  def deploy_error(step)
    text = <<~SLACK_MESSAGE.strip
      #{header} Deployment of #{gitlab_info} stopped on #{step.class} after #{total_time} :fine:
    SLACK_MESSAGE

    fire_hook(text, attachment: truncated_message(step.error))
  end

  def qa_notification
    return unless @environment == 'gstg'

    text = <<~SLACK_MESSAGE.strip
      #{header} GitLab *v#{@version}* has been deployed to *#{@environment}*. Find the QA issue <https://gitlab.com/gitlab-org/release/tasks/issues?label_name[]=QA%20task|here>.
    SLACK_MESSAGE

    Takeoff.config[:qa_notification_channels].each do |channel|
      fire_hook(text, channel: "#{channel}")
    end
  end

  def migrations(type, duration)
    fire_hook("#{header} #{type} took *#{duration}* to run on *#{@environment}*")
  end

  def migration_error(type, duration, error)
    text = "#{header} #{type} stopped after *#{duration}* on *#{@environment}*"

    fire_hook(text, attachment: truncated_message(error))
  end

  private

  def header
    @header ||= "_#{@shared_status.user}_"
  end

  def gitlab_info
    @gitlab_info ||= "GitLab *v#{@version}* to *#{@environment}*"
  end

  def total_time
    @shared_status.total_time
  end

  def fast_emoticon
    @shared_status.long_time? ? ':turtle:' : ':zap:'
  end

  def truncated_message(message)
    message.size > TRUNCATE_MESSAGE_SIZE ? "#{message[0...TRUNCATE_MESSAGE_SIZE]}..." : message
  end

  def fire_hook(text, attachment: nil, channel: Takeoff.config[:slack_channel])
    return if Takeoff.config[:dry_run] || !Takeoff.config[:slack_enabled] || ENV['RACK_ENV'] == 'test'

    body = { text: text }
    body[:channel] = channel
    body[:attachments] = [{ text: attachment, color: 'danger' }] if attachment
    response = HTTParty.post(@url, body: body.to_json)

    raise CouldNotPostError, response.inspect unless response.code == 200
  end
end
