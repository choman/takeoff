# frozen_string_literal: true

require './config/takeoff'
require './lib/pid'

module StopDeploy
  WAIT_ATTEMPTS = 10
  WAIT_SECONDS = 3

  def self.stop
    pid = File.read(Takeoff.config[:pidfile]).to_i

    Process.kill('SIGINT', pid)

    WAIT_ATTEMPTS.times do
      sleep WAIT_SECONDS

      return unless alive?(pid)
    end

    Pid.destroy
    Process.kill('SIGKILL', pid)
  rescue Errno::ESRCH
    puts 'No process found.'
  end

  private_class_method

  def self.alive?(pid)
    Process.getpgid(pid)

    true
  rescue Errno::ESRCH
    false
  end
end
