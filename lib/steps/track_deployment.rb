# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class TrackDeployment < BaseRoles
    def run
      # Once the whole cluster is deployed we'll track the deployment in GitLab
      # Performance Monitoring. We do this by running the Rake task on the
      # blessed worker _only_.

      run_command_on_roles roles.blessed_node,
                           'sudo gitlab-rake gitlab:track_deployment',
                           title: 'Tracking the deployment from the blessed node'
    rescue
      puts "Deployment tracking failed. I'm moving on anyway."
    end

    private
  end
end
