module Steps
  module VersionCheckUtil
    def self.version_regexp
      /Current Version:/
    end

    def self.host_and_version(line)
      line_parts = line.strip.split

      [line_parts.first] + line_parts.last(2)
    end

    def self.version_command
      'echo Current Version: `echo "$(dpkg-query --show gitlab-ee) $(hostname -f)"`'
    end
  end
end
