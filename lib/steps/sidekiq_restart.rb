# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class SidekiqRestart < BaseRoles
    SERVICES = %w[sidekiq sidekiq-cluster].freeze

    def run
      SERVICES.each do |service|
        service_roles = roles.service_roles[service]

        next unless service_roles

        run_command_on_roles service_roles,
                             "sudo gitlab-ctl restart #{service}",
                             title: "Restarting #{service} on #{Roles.short_output(service_roles)}"
      end
    end
  end
end
