# frozen_string_literal: true

require 'rspec'
require './lib/lb/config'
require './lib/lb/command_wrapper'

describe Lb::CommandWrapper do
  subject { described_class.new('gstg') }

  before do
    allow_any_instance_of(Lb::Config).to receive(:lb_list).and_return('fe-altssh-02-lb-gstg:fe-pages-01-lb-gstg')
  end

  it 'returns the command to run with the wrapper' do
    expect(subject.run_command(command: 'ls')).to eq("sudo /usr/local/bin/ha-ctl -l fe-altssh-02-lb-gstg:fe-pages-01-lb-gstg -c \"ls && sleep 3\"")
  end

  it 'returns the drain command' do
    expect(subject.drain).to eq("sudo /usr/local/bin/ha-ctl -l fe-altssh-02-lb-gstg:fe-pages-01-lb-gstg -a drain")
  end

  it 'returns the maintenance command' do
    expect(subject.maintenance).to eq("sudo /usr/local/bin/ha-ctl -l fe-altssh-02-lb-gstg:fe-pages-01-lb-gstg -a maint")
  end

  it 'returns the ready command' do
    expect(subject.ready).to eq("sudo /usr/local/bin/ha-ctl -l fe-altssh-02-lb-gstg:fe-pages-01-lb-gstg -a ready")
  end
end
